/* Crie uma função que procura por uma substring dentro um uma string. Caso o trecho
procurado exista na string, a função retorna a posição em que o trecho começa.
Caso contrário, a função deve retornar o valor -1.
Por exemplo: */

var s = "Aula de web1"

console.log(procuraSubStr(s,"web1"))
// -> 8
console.log(procuraSubStr(s,"web2"))
// -> -1

function procuraSubStr(s, sub){
    var j = 0
    subIgual = -1

    for (let index = 0; index < s.length; index++) {
        if(s[index] === sub[0]){
            for (let i = 1; i < sub.length; i++) {
                j = index + i

                if(sub[i] != s[j]){
                    subIgual = 0
                }
            }

            if(subIgual === -1){
                return index
            }else{
                subIgual === -1
            }
        }
    }

    return -1
}