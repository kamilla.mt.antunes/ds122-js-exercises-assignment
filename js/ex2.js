// Quadro 8x8

var linha1 = [" # # # #"]
var linha2 = ["# # # # "]
var str = ""

for (let index = 1; index < 9; index++) {
    if(index % 2 === 1){
        str += linha1
    }else{
        str += linha2
    }

    str += "\n"
}

console.log(str)
console.log("\n")

// Qualquer tamanho

var tam = 10
var verificaLinha = 1
var str2 = " "

for (let index = 0; index < tam; index++) {
    for (let index2 = 1; index2 < tam + 1; index2++) {
        if(index2 % 2 === verificaLinha){
            str2 += " "
        }else{
            str2 += "#"
        }
    }

    str2 += "\n"
    
    if(verificaLinha === 1) {
        verificaLinha = 0
    } else {
        verificaLinha = 1
    }
}

console.log(str2)

